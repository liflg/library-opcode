Website
=======
http://www.codercorner.com/Opcode.htm (Windows only)

http://www.paassen.tmfweb.nl/ (Linux, dead)

License
=======
custom license (see the file source/COPYING)

Version
=======
1.2.1

Source
======
OPCODE-1.2.1.zip (sha256: c245b82f40041312789f870ae1460451a6084c7ec0a957b6f615213749913803)
